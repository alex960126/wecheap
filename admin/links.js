/**
 * Created by Andrey on 14.04.2016.
 */
$("a").on("click", function(e) {
    if($(this).attr("data-rel") == "back") {
        window.history.back();
        return;
    }

    var link = $(this).attr('href');
    window.location.href=  link;
    return false;
});