$(document).ready(function($) {
  $(function() {
    // $(".button-collapse").sideNav();
    $(".button-collapse").sideNav({
      menuWidth: 300,
      edge: "left",
      closeOnClick: true,
      draggable: true,
      // isFixed: true,
      onOpen: function(el) {},
      onClose: function(el) {}
    });
  });
  $('.carousel').carousel();
  var $menu = $(".nav-profile");
 $(window).scroll(function () {
  if ($(this).scrollTop() > 50) {
    $menu.css({
      'background-color': '#d9d9d9',
    })
  } else if ($(this).scrollTop() <= 100) {
    $menu.css({
      'background-color': 'transparent',
    })
  }

 });
});

$(".reset").click(function() {
  $(this)
    .closest("form")
    .find("input[type=text], textarea")
    .val("");
});

var nonLinearSlider = document.getElementById("test-slider");

noUiSlider.create(nonLinearSlider, {
  connect: true,
  behaviour: "tap",
  start: [500, 4000],
  step: 1,
  range: {
    min: 0,
    max: 10000
  },
  format: wNumb({
    decimals: 1,
    thousand: ".",
    suffix: "₽"
  })
});

var nodes = [
  document.getElementById("lower-value"), // 0
  document.getElementById("upper-value") // 1
];

// Display the slider value and how far the handle moved
// from the left edge of the slider.
nonLinearSlider.noUiSlider.on("update", function(
  values,
  handle,
  unencoded,
  isTap,
  positions
) {
  nodes[handle].innerHTML = values[handle];
});

$(function() {
  $('.filter-hide').click(function(){
     $('.filter-select').stop().fadeToggle();
     $('.filter-btn').stop().fadeToggle();
     $('.search-hide').stop().fadeToggle();
   });
});
