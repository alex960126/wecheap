$(document).ready(function($) {
  $(function() {
    // $(".button-collapse").sideNav();
    $(".button-collapse").sideNav({
      menuWidth: 300,
      edge: "left",
      closeOnClick: true,
      draggable: true,
    });
  });
  // $(".product_select").formSelect();
  // $('select').formSelect();
});

$(".reset").click(function() {
  $(this)
    .closest("form")
    .find("input[type=text], textarea")
    .val("");
});

$(function() {
     $('.filter-hide').click(function(){
        $('.filter-select').stop().fadeToggle();
        $('.filter-btn').stop().fadeToggle();
      });
  });

$(function() {
     $('.search-hide-btn').click(function(){
        $('.search-hide').stop().fadeToggle();
        $('.filter').stop().fadeToggle();
        $('.filter-select').stop().fadeToggle();
        $('.filter-btn').stop().fadeToggle();
      });
  });
/**
 * Применение фильтров.
 */
function filter() {
    var viscosity = $('#filter-viscosity').val();
    var volume = $('#filter-volume').val();
    var appointment = $('#filter-appointment').val();
    var manufacturer = $('#filter-manufacturer').val();

    viscosity = viscosity !== null ? viscosity : '';
    volume = volume !== null ? volume : '';
    appointment = appointment !== null ? appointment : '';
    manufacturer = manufacturer !== null && manufacturer != undefined ? manufacturer : '';

    localStorage.removeItem('category');
    localStorage.removeItem('vendor');

    window.location.replace(
        '/m/products.html?viscosity='
        + viscosity
        + '&volume='
        + volume
        + '&appointment='
        + appointment
        + '&manufacturer='
        + manufacturer
    );
}
  