var token = localStorage.getItem('api_token');

if (token != null) {
    var profileUrl = $('#profile-url');

    profileUrl.addClass('authorized');
    profileUrl.attr('href', '/m/profile1.html');

    $.post(
        'https://2.app-wecheap.ru/api/v1/account',
        {api_token: token},
        function (user) {
            if (user.length < 1) return;

            if (user.avatar) {
                $('#nav-small-avatar').attr('src', user.avatar);
                $('#nav-background-avatar').attr('src', user.avatar);
            }

            $('#login-url').remove();
            $('#nav-user-info').append('<a href="profile1.html">' +
                '<span class="white-text name">Профиль</span>' +
                '</a>' +
                '<a onclick="logout(); window.location.reload()">' +
                '<span class="white-text name">Выйти</span>' +
                '</a>');
        }
    );
}

function logout() {
    localStorage.removeItem('api_token');
    location.reload();
}