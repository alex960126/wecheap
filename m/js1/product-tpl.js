productTpl = '<div class="col s6 m4 l4">\n' +
    '    <div id="${id}" class="product">\n' +
    '        <div class="product_img">\n' +
    '            <img class="responsive-img" src="${img}" alt="">\n' +
    '        </div>\n' +
    '        <div class="product_bottom">\n' +
    '            <span class="product_bottom-name">\n' +
    '                <a href="product.html?id=${id}">${name}</a>\n' +
    '            </span>\n' +
    '            <div class="single-product_cart-property_list qty">\n' +
    '                <select class="browser-default product_select"> </select>\n' +
    '            </div>\n' +
    '            <span class="product_bottom-price">\n' +
    '                <p>Цена:</p> ${price}  <p>₽</p>\n' +
    '            </span>\n' +
    '            <a onclick="cart.addToCartFromProductsPage(event, ${id}, {img : \'${img}\', price : \'${price}\', name : \'${name}\'})" class="product_bottom-btn" href="#!">В корзину</a>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>';
