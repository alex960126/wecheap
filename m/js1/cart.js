function Cart() {
    this.sendOrder = function () {
        if (!this.validate()) {
            return;
        }

        var order = {
                name            : this.getCustomerName(),
                phone           : this.getCustomerPhone(),
                deliveryType    : this.getDeliveryType(),
                deliveryRegion  : this.getDeliveryRegion(),
                deliveryCompany : this.getDeliveryCompany(),
                address         : this.getCustomerAddress(),
                porch           : this.getCustomerPorch(),
                floor           : this.getCustomerFloor(),
                payType         : this.getCustomerPayType(),
                promo           : this.getCustomerPromo(),
                items           : this.getCart(),
                api_token       : this.getCustomerApiToken()
        };


        $.ajax({
            url     : 'https://2.app-wecheap.ru/api/v1/new',
            method  : 'post',
            data    : order,
            success : function (response) {
                cart.clearCart();
                $('body').html(response);
                setTimeout("window.location.replace('/m/index.html')",5000);
            }
        });
    };

    this.renderCart = function (container, itemTemplate) {
        var cart = this.getCart() || {};
        var itemsCount = Object.values(cart).length;

        if (itemsCount > 0) {
            this.hideEmpty();

            for (uniqueId in cart) {
                var item = cart[uniqueId];
                item.uniqueId = uniqueId;

                $.tmpl(itemTemplate, item).appendTo(container);
            }

            this.updateOrderInfo();
            this.showOrderInfo()
        } else {

        }
    };

    this.updateOrderInfo = function () {
        var amount = this.getCartAmount();

        $('#cart-amount').text(String(amount) + ' Ք');
        $('#itog').text('Итого: ' + String(amount + this.getDeliveryCost()) + ' Ք');


        this.validate();
    };

    this.validate = function () {
        var amount = this.getCartAmount();
        if (parseInt(amount) < parseInt(settings.setting[1].value)) {
            $('#sendBusketButton').attr('disabled', 'disabled');
            $('#errors').text('Минимальная сумма заказа - ' +settings.setting[1].value);

            return false;
        }

        $('#sendBusketButton').removeAttr('disabled');

        return true;
    };

    this.showEmpty = function () {
        $('#empty-cart').show();
    };

    this.hideEmpty = function () {
        $('#empty-cart').hide();
    };

    this.showOrderInfo = function () {
        $('#order-info').show();
    };

    this.hideOrderInfo = function () {
        $('#order-info').hide();
    };

    this.showOrderForm = function () {
        $('#order').show();
    };

    this.addToCartFromProductsPage = function (e, id, options) {
        var cart = this.getCart() || {};

        var qty = $(e.target).siblings('.qty').find('select').val();

        console.log(qty);

        if (cart.hasOwnProperty(id)) {
            cart[id].qty = qty;
        } else {
            cart[id] = {
                qty: qty,
                options: options
            };
        }

        this.setCart(cart);
        this.itemAddedNotificate();
    }

    this.addToCart = function (e, id, options) {
        var cart = this.getCart() || {};
        var qty = $('#qty > select').val();

        if (cart.hasOwnProperty(id)) {
            cart[id].qty = qty;
        } else {
            cart[id] = {
                qty: qty,
                options: options
            };
        }

        this.setCart(cart);
        this.itemAddedNotificate();
    };

    this.changeQty = function (id, qty) {
        var cart = this.getCart();

        if (!cart) {
            return false;
        }

        if (!cart.hasOwnProperty(id)) {
            return false;
        }

        cart[id].qty = qty;
        this.setCart(cart);

        this.updateOrderInfo();

        return true;
    };

    this.removeFromCart = function (e, id) {
        var cart = this.getCart();

        if (!cart) {
            return false;
        }

        if (!cart.hasOwnProperty(id)) {
            return false
        }

        delete cart[id];

        this.setCart(cart);

        if (Object.values(cart).length < 1) {
            this.hideOrderInfo();
            this.showEmpty();
        }

        var itemContainer = $(e.target).closest('.card_body');

        itemContainer.hide('fade', function () {
            itemContainer.remove()
        });

        this.updateOrderInfo();
    };

    this.getCartAmount = function () {
        var amount = 0;
        var cart = this.getCart() || {};
        cart = Object.values(cart);

        cart.forEach(function (item) {
            amount += parseInt(item.options.price) * parseInt(item.qty);
        });

        return amount;
    };

    this.getDeliveryCost = function () {
        var cost = 0;

        if (!this.isEmsDelivery()) {
            $('#ems-info-block').hide('fade');
            cost = $('#delivery-region :selected').data('price');
        } else {
            $('#ems-info-block').show('fade');
        }

        return cost;
    };

    this.getCart = function () {
        return JSON.parse(localStorage.getItem('cart'));
    };

    this.setCart = function (cart) {
        localStorage.setItem('cart', JSON.stringify(cart));
    };

    this.clearCart = function () {
        localStorage.removeItem('cart')
    };

    this.itemAddedNotificate = function () {
        $('#mini-cart').addClass('pulse');
    };

    this.getCustomerName = function () {
        return $('#name').val();
    };

    this.getCustomerPhone = function () {
        return $('#phone').val();
    };

    this.getCustomerAddress = function () {
        return $('#place').val();
    };

    this.getCustomerPorch = function () {
        return $('#porch').val();
    };

    this.getCustomerFloor = function () {
        return $('#floor').val();
    };


    this.getDeliveryType = function () {
        return $('#delivery-type').val();
    };

    this.getDeliveryRegion = function () {
        return $('#delivery-region').val();
    };

    this.getDeliveryCompany = function () {
        return $('#delivery-company').val();
    };

    this.getCustomerPayType = function () {
        return $('[name=pay_type]:checked').val();
    };

    this.getCustomerPromo = function () {
        return $('#promo').val();
    };

    this.getCustomerApiToken = function () {
        return null !== localStorage.getItem('api_token') ? localStorage.getItem('api_token') : false;
    };

    this.isEmsDelivery = function () {
        return 2 == this.getDeliveryType();
    }
}