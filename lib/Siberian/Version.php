<?php

/**
 * Class Siberian_Version
 */
class Siberian_Version
{
    const TYPE = 'SAE';
    const NAME = 'Single App Edition';
    const VERSION = '4.14.11';
    const NATIVE_VERSION = '8';
    const API_VERSION = '4';

    /**
     * @param $type
     * @return bool
     */
    static function is($type)
    {
        return self::TYPE == strtoupper($type);
    }
}
